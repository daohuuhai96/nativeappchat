import {StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
// import RNIcon from 'react-native-vector-icons/Ionicons';
import SideMenu from 'react-native-side-menu-updated';
// import Login from '../Auth/Login';

import {Header, RNView, RNButton, RNImage, RNInput, RNText} from 'components';
import Menu from 'screens/SideMenu';

import {COLORS, IMAGES} from 'themes';

// import {translate} from 'translate';
import {Navigation} from 'react-native-navigation';
// import {Alert, AppRegistry, Button, View} from 'react-native';
import {push} from 'navigations';
const Home = ({componentId}) => {
  const [isMenu, setMenu] = useState(false);
  const menu = <Menu />;

  const onChangeMenu = () => {
    setMenu(!isMenu);
  };
  const login = () => {
    push({screen: 'Login', id: componentId});
  };

  useEffect(() => {
    Navigation.mergeOptions(componentId, {
      bottomTabs: {
        visible: isMenu ? false : true,
        drawBehind: isMenu ? false : true,
      },
    });
  }, [isMenu, componentId]);

  return (
    <SideMenu
      menu={isMenu ? menu : null}
      isOpen={isMenu}
      onChange={onChangeMenu}>
      <RNView fill>
        <RNView
          h={130}
          color={COLORS.primary}
          borderBottomRightRadius={32}
          borderBottomLeftRadius={32}>
          <Header
            iconLeft={'menu-outline'}
            onPressBack={onChangeMenu}
            componentId={componentId}
            sizeIcon={32}
            colorIcon={COLORS.bgPage}
          />
        </RNView>
        <RNView center pTop={50}>
          <RNImage source={IMAGES.imgLogo} w={150} h={150} />
        </RNView>
        <RNView center pTop={50}>
          <RNText>
            {
              'Vui lòng để lại ý kiến cho chúng tôi ở bên dưới nếu bạn có bất kỳ câu hỏi nào.'
            }
          </RNText>
        </RNView>

        <RNView pHoz={8} column pTop={20}>
          <RNInput
            mLeft={10}
            mRight={10}
            // label={translate('AUTH.email')}
            placeholder={'Nội dung'}
            // value={formik.values.email}
            // onChangeText={'Nội dung'}
            // touched={formik.touched.email}
            // errorMessage={formik.errors.email}
          />
          <RNView center pHoz={50}>
            <RNButton onPress={login} title="Gửi" />
          </RNView>
        </RNView>
      </RNView>
    </SideMenu>
  );
};

export default Home;

const styles = StyleSheet.create({});
