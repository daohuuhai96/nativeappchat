import React, {useState, useCallback, useEffect, useRef} from 'react';
import {GiftedChat, Composer, Send} from 'react-native-gifted-chat';
import {StyleSheet} from 'react-native';
import RNIcon from 'react-native-vector-icons/Ionicons';
import {RNView, Header, RNTouchable, RNContainer} from 'components';
import {COLORS} from 'themes';
import axios from 'axios';
const io = require('socket.io-client/dist/socket.io');

const ChatBox = ({componentId}) => {
  const [messages, setMessages] = useState([]);
  console.log('dataa ', messages);
  const [isMessage, setIsMessage] = useState(false);
  const [joined, setJoined] = useState(false);
  // const socket = io(`http://localhost:8000`, {
  //   transports: ['websocket'], // you need to explicitly tell it to use websockets
  // });

  const socketRef = useRef();

  useEffect(() => {
    getMessages();
    setJoined(true);
    socketRef.current = io.connect(`http://localhost:8000`, {
      transports: ['websocket'], // you need to explicitly tell it to use websockets
    });
    socketRef.current.on('connect', () => {
      console.log('connect socket');
    });

    socketRef.current.on('message:received', message => {
      console.log('message:received', message);
      setMessages(messages => [message, ...messages]);
    });

    socketRef.current.on('newMessageAll', text => {
      console.log('mess', text);
      // setMessages(previousMessages =>
      //   GiftedChat.append(previousMessages, message),
      // );
    });
    return () => {
      socketRef.current.disconnect();
    };
  }, []);
  async function getMessages() {
    const response = await axios.post(
      'http://localhost:8000/v2/message/allMessage',
      {
        userID: '62ceeabb3d573c593',
        page: 0,
      },
    );
    const messageAll = Object.values(response.data);
    setMessages(messageAll);
  }
  const onSend = useCallback(mess => {
    if (mess[0]?.text?.length < 1) return;
    const message = {
      createdAt: new Date(),
      userID: '62ceeabb3d573c593',
      email: 'aldjals',
      text: mess[0].text,
      idUserOther: 'adkjlkasjd',
    };
    // setMessages(messages.concat(message));
    socketRef.current.emit('message', message);
    // socket.on('newMessageAll', text => {
    //   setMessages(previousMessages =>
    //     GiftedChat.append(previousMessages, message),
    //   );
    //   // setIsMessage(false);
    // });
  });

  // {
  // _id: message?._id,
  // text: message?.msg || message?.text,
  // createdAt: message?.timeCmt || message?.createdAt,
  // timeCmt:message?.timeCmt || message?.createdAt,
  // user: {
  //   _id: this.state.user_id,
  //   name: message?.name || message?.user?.name,
  //   avatar:
  //     message?.avatar ||
  //     message?.user?.avatar ||
  //     'https://hocmai.vn/images/avatar/avatar_5.png',
  // },
  // };

  // const onSends = useCallback((messages = []) => {
  //   setMessages(previousMessages =>
  //     GiftedChat.append(previousMessages, messages),
  //   );
  //   setIsMessage(false);
  // }, []);
  useEffect(() => {
    setIsMessage(false);
  }, []);

  const renderComposer = props => {
    return (
      <RNView row center fill borderRadius={1} borderColor={COLORS.primary}>
        {!isMessage && (
          <RNTouchable
            mHoz={12}
            center
            w={28}
            h={28}
            borderWidth={1}
            borderColor={COLORS.primary}
            borderRadius={4}>
            <RNIcon name="camera-outline" size={20} color={COLORS.primary} />
          </RNTouchable>
        )}
        <Composer {...props} style={styles.chatInput} />
        {!isMessage && (
          <RNTouchable mHoz={12} center w={30} h={30}>
            <RNIcon name="heart" size={28} color={COLORS.primary} />
          </RNTouchable>
        )}
      </RNView>
    );
  };
  const renderIconSend = props => {
    return (
      <Send {...props} containerStyle={styles.sendContainer}>
        <RNIcon
          name="send-outline"
          size={24}
          color={COLORS.primary}
          style={styles.icSend}
        />
      </Send>
    );
  };

  return (
    <RNView fill>
      <Header
        title={'User'}
        componentId={componentId}
        icon={'trash'}
        onPressRight={() => {}}
        colorIconRight={COLORS.error}
        sizeIconRight={20}
      />
      <RNContainer>
        <GiftedChat
          messages={messages && messages}
          onSend={message => onSend(message)}
          onInputTextChanged={() => setIsMessage(true)}
          renderComposer={renderComposer}
          user={{
            _id: '62ceeabb3d573c593',
          }}
          renderSend={renderIconSend}
          optionTintColor={COLORS.primary}
          messagesContainerStyle={styles.message}
        />
      </RNContainer>
    </RNView>
  );
};

const styles = StyleSheet.create({
  chatInput: {
    flex: 1,
    borderRadius: 1,
    borderColor: COLORS.primary,
  },
  icSend: {
    marginLeft: 30,
  },
  sendContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 18,
  },
});

export default ChatBox;
