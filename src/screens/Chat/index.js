import {StyleSheet, SafeAreaView} from 'react-native';
import React from 'react';
import {Header, RNImage, RNText, RNTouchable, RNView} from 'components';

import {push} from 'navigations';
import {COLORS, FONTS, IMAGES, TYPES} from 'themes';
import axios from 'axios';

const Chat = ({componentId}) => {
  // useEffect(() => {
  //   const socket = socketIO('http://192.168.0.8:5000', {
  //     transports: ['websocket'],
  //     jsonp: false,
  //   });
  //   socket.connect();
  //   socket.on('connect', () => {
  //     console.log('connected to socket server');
  //   });
  // }, []);

  const onChat = async () => {
    console.log('log1');
    await axios.post('http://localhost:8000/v2/message/changeState', {
      isActive: true,
      email: 'daohuuhai961@gmail.com',
    });
    await setTimeout(async () => {
      console.log('log2');
      var response = await axios.post(
        'http://localhost:8000/v2/message/getRoom',
        {
          id: '62dc2c6569afe3815b95f554',
          email: 'daohuuhai961@gmail.com',
        },
      );
      console.log(response);
      console.log('log3');
      await axios.post('http://localhost:8000/v2/message/changeState', {
        isActive: false,
        email: 'daohuuhai961@gmail.com',
      });
      if (response.data) {
        alert('ghep doi thanh cong');
      }
      push({screen: 'ChatBox', id: componentId});
    }, 5000);
  };
  return (
    <SafeAreaView style={styles.container}>
      <Header title={'Tin Nhắn'} mTop={1} />
      <RNView fill pHoz={28}>
        <RNTouchable
          row
          pHoz={8}
          pVer={12}
          onPress={onChat}
          style={styles.containerChat}>
          <RNView
            borderRadius={25}
            h={50}
            w={50}
            borderWidth={1}
            borderColor={COLORS.primary}>
            <RNImage source={IMAGES.icApple} />
          </RNView>
          <RNView mLeft={12} fill>
            <RNText
              color={COLORS.black}
              type={TYPES.bold}
              size={FONTS.primary}
              lines={1}
              fill
              mTop={4}>
              {'Chát cùng người lạ.'}
            </RNText>
            <RNText
              type={TYPES.regular}
              size={FONTS.primary}
              color={COLORS.primaryText}
              lines={1}
              fill>
              {'Tham gia ghép đôi ngẫu nhiên.'}
            </RNText>
          </RNView>
        </RNTouchable>
      </RNView>
    </SafeAreaView>
  );
};

export default Chat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerChat: {
    shadowColor: '#000',
    backgroundColor: COLORS.bgPage,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 6,
    marginTop: 20,
  },
});
