import {combineReducers} from '@reduxjs/toolkit';
import general from './general';
import auth from './auth';

export const reducers = combineReducers({
  general: general,
  auth: auth,
});
